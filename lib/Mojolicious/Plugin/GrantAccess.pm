package Mojolicious::Plugin::GrantAccess;
use Mojo::Base 'Mojolicious::Plugin';

our $VERSION = '0.09';

sub register {

  my ( $self, $app ) = @_;

  if (defined($app->config('ldap')) || defined($app->config('htpasswd'))) {

    $app->sessions->default_expiration($app->config('session_duration'));

    if (defined($app->config('ldap'))) {
      require Net::LDAP;
    }
    if (defined($app->config('htpasswd'))) {
      require Apache::Htpasswd;
      die sprintf('Unable to read %s', $app->config('htpasswd')) unless -r $app->config('htpasswd');
    }
    $app->plugin('Authentication' => {
      autoload_user => 1,
      session_key   => ucfirst($app->moniker),
      load_user     => sub {
        my ($c, $user) = @_;

        if (defined $app->config('load_user_method')) {
          $user = $app->config('load_user_method')->($user, $app);
        }

        return $user;
      },
      validate_user => sub {
        my ($c, $username, $password, $extradata) = @_;

        if ($c->config('ldap')) {
          my $user = {};

          my $ldap             = Net::LDAP->new($c->config('ldap')->{uri});
          my $ldap_user_attr   = $c->config('ldap')->{user_attr};
          my $ldap_user_filter = $c->config('ldap')->{user_filter};
          my $mesg;

          # Start TLS options
          $mesg = $ldap->start_tls($c->config('ldap')->{start_tls}) if $c->config('ldap')->{start_tls};

          # LDAP binding
          if (defined($c->config('ldap')->{bind_dn}) && defined($c->config('ldap')->{bind_pwd})) {
            # connect to the ldap server using the bind credentials
            $mesg = $ldap->bind(
              $c->config('ldap')->{bind_dn},
              password => $c->config('ldap')->{bind_pwd}
             );
          } else {
            # anonymous bind
            $mesg = $ldap->bind;
          }

          # Has LDAP binding succeed?
          if ($mesg->code) {
            $c->app->log->info(sprintf(
              '[LDAP INFO] Authenticated bind failed - Login %s',
              $c->config->{ldap}->{bind_dn}
             )) if defined($c->config('ldap')->{bind_dn});
            $c->app->log->error(sprintf('[LDAP ERROR] Error on bind: %s', $mesg->error));
            return undef;
          }

          # Search the ldap database for the user who is trying to login
          $mesg = $ldap->search(
            base   => $c->config('ldap')->{user_tree},
            filter => sprintf('(&(%s=%s)%s)', $ldap_user_attr, $username, $ldap_user_filter)
           );

          # Errorless search?
          if ($mesg->code) {
            $c->app->log->error(sprintf('[LDAP ERROR] Error on search: %s', $mesg->error));
            return undef;
          }

          # Check to make sure that the ldap search returned at least one entry
          my @entries = $mesg->entries;
          my $entry   = $entries[0];
          unless (defined $entry) {
            $c->app->log->info(sprintf('LDAP INFO] Authentication failed - User %s filtered out, ', $username ));
            return undef;
          }

          # Retrieve the first user returned by the search
          $c->app->log->debug(sprintf('[LDAP DEBUG] Found user dn: %s', $entry->dn));

          # Now we know that the user exists
          $mesg = $ldap->bind(
            $entry->dn,
            password => $password
           );

          # Was it the good password?
          if ($mesg->code) {
            $c->app->log->info( '[LDAP INFO] Authentication failed - Login: ' . $username );
            $c->app->log->error( '[LDAP ERROR] Authentication failed: ' . $mesg->error);
            return undef;
          }

          if ($app->config('ldap_map_attr')) {
            while (my ($key, $attr) = each %{$app->config('ldap_map_attr')}) {
              next if $key eq 'username';
              if ($entry->exists($attr)) {
                $user->{$key} = $entry->get_value($attr);
              }
            }
            $c->debug($user);
          }
          $c->app->log->info(sprintf('[LDAP INFO] Authentication successful - Login: %s', $username ));

          $user->{username} = $username;
          return $user;
        } elsif ($c->config('htpasswd')) {
          my $htpasswd = new Apache::Htpasswd({
            passwdFile => $c->config('htpasswd'),
            ReadOnly   => 1
           });
          if (!$htpasswd->htCheckPassword($username, $password)) {
            return undef;
          }
          $c->app->log->info(sprintf('[Simple authentication successful] login: %s', $username ));
          return { username => $username };
        }
      }
     });
  }
}

1;
__END__

=encoding utf8

=head1 NAME

Mojolicious::Plugin::GrantAccess - Mojolicious Plugin for easy
authentication

=head1 SYNOPSIS

  # Mojolicious
  $self->plugin('GrantAccess');

  # Mojolicious::Lite
  plugin 'GrantAccess';

=head1 DESCRIPTION

L<Mojolicious::Plugin::GrantAccess> is a L<Mojolicious> plugin that
makes simplier to authenticate with C<LDAP> or with basic
authentication and a C<htpasswd> file. It makes an heavy use of
L<Mojolicious::Plugin::Authentication> but add some features for it's
integration with L<Net::LDAP> or L<Apache::htpasswd>.

It's designed to be integrated in a larger L<Mojolicious> application.

=head1 CONFIGURATION

Your app should set up a session.



This plugin will require two optionnal dependencies that should be
installed from your main app:

=over 2

=item L<Net::LDAP>

=item L<Apache::htpasswd>

=back

Depending on the setup you will choose, feel free to add it to your
app C<cpanfile>

  feature 'ldap', 'LDAP authentication support' => sub {
    requires 'Net::LDAP';
    requires 'Mojolicious::Plugin::Authentication';
    requires 'Date::Language';
  };

  feature 'htpasswd', 'Htpasswd authentication support' => sub {
    requires 'Apache::Htpasswd';
    requires 'Mojolicious::Plugin::Authentication';
  };


=head2 Basic authentication

  # You will be prompted for a password twice
  $ htpasswd -c grantaccess.passwd smonff

In your Mojolicious app configuration , you have to insert a line
containing the path of the .htpasswd file:

=head2 LDAP

TODO

=head1 METHODS

L<Mojolicious::Plugin::GrantAccess> inherits all methods from
L<Mojolicious::Plugin> L<Mojolicious::Plugin::Authentication>.

A common pattern is to use C<Mojolicious::Plugin::Authentication>'s
C<is_user_authenticated()> method to check if the user is
authenticated from the controller:

  $c->redirect_to('/login') unless $c->is_user_authenticated;

It can as well be used in templates, like in this example for
implementing a login/logout button depending of the context:

  <li>
    % if ( is_user_authenticated() ) {
      %= form_for '/logout' => ( method => 'POST' ) => begin
        %= csrf_field
        %= submit_button 'Logout'
      % end
    % } else {
      % if ( not current_route 'login' ) {
        %=  $button->('Login', 'success', '/login')
      % }
    % }
  </li>

It implements the following new methods.

=head2 register

  $plugin->register(Mojolicious->new);

Register plugin in L<Mojolicious> application.

=head1 SEE ALSO

A good example of use of the original version of this module can be
found in L<Lufi|https://framagit.org/fiat-tux/hat-softwares/lufi>

L<Mojolicious::Plugin:Authentication>, L<Mojolicious>,
L<Mojolicious::Guides>, L<https://mojolicious.org>.

=head1 AUTHOR

Luc Didry C<< <ldidry@cpan.org> >> wrote the original version.

Sébastien Feugère C<< <sebastien@feugere.net> >> initiated this fork.

This module was originally written by Luc Didry (not released on CPAN
but L<available on
Framagit|https://framagit.org/fiat-tux/mojolicious/fiat-tux/mojolicious-plugin-fiattux-grantaccess/>).

It got modified by Sébastien Feugère for the needs of the
L<IFOP|https://www.ifop.com> survey company and L<made available on
Gitlab>|https://gitlab.com/smonff/mojolicious-plugin-grantaccess,
though not published on CPAN yet.

=head1 LICENSE

Mojolicious-Plugin-FiatTux-GrantAccess
Copyright (C) 2019  Fiat Tux / Mojolicious plugins / Fiat Tux

Mojolicious-Plugin-GrantAccess
Copyright (C) 2019  Sébastien Feugère

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.


=cut
