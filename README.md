# NAME

Mojolicious::Plugin::GrantAccess - Mojolicious Plugin for easy
authentication

# SYNOPSIS

    # Mojolicious
    $self->plugin('GrantAccess');

    # Mojolicious::Lite
    plugin 'GrantAccess';

# DESCRIPTION

[Mojolicious::Plugin::GrantAccess](https://metacpan.org/pod/Mojolicious::Plugin::GrantAccess) is a [Mojolicious](https://metacpan.org/pod/Mojolicious) plugin that
makes simplier to authenticate with `LDAP` or with basic
authentication and a `htpasswd` file. It makes an heavy use of
[Mojolicious::Plugin::Authentication](https://metacpan.org/pod/Mojolicious::Plugin::Authentication) but add some features for it's
integration with [Net::LDAP](https://metacpan.org/pod/Net::LDAP) or [Apache::htpasswd](https://metacpan.org/pod/Apache::htpasswd).

It's designed to be integrated in a larger [Mojolicious](https://metacpan.org/pod/Mojolicious) application.

# CONFIGURATION

Your app should set up a session.

This plugin will require two optionnal dependencies that should be
installed from your main app:

- [Net::LDAP](https://metacpan.org/pod/Net::LDAP)
- [Apache::htpasswd](https://metacpan.org/pod/Apache::htpasswd)

Depending on the setup you will choose, feel free to add it to your
app `cpanfile`

    feature 'ldap', 'LDAP authentication support' => sub {
      requires 'Net::LDAP';
      requires 'Mojolicious::Plugin::Authentication';
      requires 'Date::Language';
    };

    feature 'htpasswd', 'Htpasswd authentication support' => sub {
      requires 'Apache::Htpasswd';
      requires 'Mojolicious::Plugin::Authentication';
    };

## Basic authentication

    # You will be prompted for a password twice
    $ htpasswd -c grantaccess.passwd smonff

In your Mojolicious app configuration , you have to insert a line
containing the path of the .htpasswd file:

## LDAP

TODO

# METHODS

[Mojolicious::Plugin::GrantAccess](https://metacpan.org/pod/Mojolicious::Plugin::GrantAccess) inherits all methods from
[Mojolicious::Plugin](https://metacpan.org/pod/Mojolicious::Plugin) [Mojolicious::Plugin::Authentication](https://metacpan.org/pod/Mojolicious::Plugin::Authentication).

A common pattern is to use `Mojolicious::Plugin::Authentication`'s
`is_user_authenticated()` method to check if the user is
authenticated from the controller:

    $c->redirect_to('/login') unless $c->is_user_authenticated;

It can as well be used in templates, like in this example for
implementing a login/logout button depending of the context:

    <li>
      % if ( is_user_authenticated() ) {
        %= form_for '/logout' => ( method => 'POST' ) => begin
          %= csrf_field
          %= submit_button 'Logout'
        % end
      % } else {
        % if ( not current_route 'login' ) {
          %=  $button->('Login', 'success', '/login')
        % }
      % }
    </li>

It implements the following new methods.

## register

    $plugin->register(Mojolicious->new);

Register plugin in [Mojolicious](https://metacpan.org/pod/Mojolicious) application.

# SEE ALSO

A good example of use of the original version of this module can be
found in [Lufi](https://framagit.org/fiat-tux/hat-softwares/lufi)

[Mojolicious::Plugin:Authentication](https://metacpan.org/pod/Mojolicious::Plugin:Authentication), [Mojolicious](https://metacpan.org/pod/Mojolicious),
[Mojolicious::Guides](https://metacpan.org/pod/Mojolicious::Guides), [https://mojolicious.org](https://mojolicious.org).

# AUTHOR

Luc Didry `<ldidry@cpan.org>` wrote the original version.

Sébastien Feugère `<sebastien@feugere.net>` initiated this fork.

This module was originally written by Luc Didry (not released on CPAN
but [available on
Framagit](https://framagit.org/fiat-tux/mojolicious/fiat-tux/mojolicious-plugin-fiattux-grantaccess/)).

It got modified by Sébastien Feugère for the needs of the
[IFOP](https://www.ifop.com) survey company and ["made available on
Gitlab"](#made-available-on-gitlab)|https://gitlab.com/smonff/mojolicious-plugin-grantaccess,
though not published on CPAN yet.

# LICENSE

Mojolicious-Plugin-FiatTux-GrantAccess
Copyright (C) 2019  Fiat Tux / Mojolicious plugins / Fiat Tux

Mojolicious-Plugin-GrantAccess
Copyright (C) 2019  Sébastien Feugère

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see
&lt;http://www.gnu.org/licenses/>.
