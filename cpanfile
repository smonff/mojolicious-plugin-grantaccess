requires 'Mojolicious', '';
requires 'Mojolicious::Plugin::Authentication';

on build => sub {
    requires 'ExtUtils::MakeMaker';
};
